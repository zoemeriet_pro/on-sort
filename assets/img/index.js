// Spectacles
import Angele from "./angele.png";
import Jokair from "./jokair.png";
import LadyDo from "./lady_do.png";

// Bars
import Graslin from "./graslin.png";
import Kolocs from "./kolocs.png";

// Films
import BirdsOfPrey from "./birds_of_prey.png";
import VoyageDrDolittle from "./voyage_dr_dolittle.png";

// Activities
import Bowling from "./bowling.png";
import Karaoke from "./karaoke.png";

// More
import Background from "./background.png";

export default {
  angele: Angele,
  jokair: Jokair,
  ladyDo: LadyDo,

  graslin: Graslin,
  kolocs: Kolocs,

  birdsOfPrey: BirdsOfPrey,
  voyageDrDolittle: VoyageDrDolittle,
  bowling: Bowling,
  karaoke: Karaoke,

  background: Background
};
