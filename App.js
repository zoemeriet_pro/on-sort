import React from "react";
import { Router, Stack, Tabs, Scene } from "react-native-router-flux";

import { AppLoading } from "expo";
import * as Font from "expo-font";

import CustomTabBar from "./src/components/CustomTabBar";
import HomePage from "./src/pages/HomePage";
import Listing from "./src/pages/Listing";
import Details from "./src/pages/Details";
import Map from "./src/pages/Map";

// Config
import colors from "./src/config/colors";

console.disableYellowBox = true;

export default function App() {
  const [isReady, setIsReady] = React.useState(false);

  React.useEffect(() => {
    const load = async () => {
      await Font.loadAsync({
        rubikLight: require("./assets/fonts/Rubik-Light.ttf"),
        rubik: require("./assets/fonts/Rubik-Regular.ttf"),
        rubikMedium: require("./assets/fonts/Rubik-Medium.ttf"),
        rubikBold: require("./assets/fonts/Rubik-Bold.ttf")
      });

      setIsReady(true);
    };
    load();
  }, []);

  if (!isReady) {
    return <AppLoading />;
  }

  return (
    <Router>
      <Tabs
        key="root"
        tabBarComponent={CustomTabBar}
        headerTransparent={true}
        backButtonTextStyle={{
          color: colors.grey
        }}
        backButtonTintColor={colors.purple}
        headerTitleStyle={{ color: "transparent" }}
      >
        <Stack key="Home" icon="list-ul">
          <Scene key="homepage" component={HomePage} initial />
          <Scene key="details" component={Details} />
          <Scene key="listing" component={Listing} />
        </Stack>

        <Stack key="Map" icon="map-marker-alt">
          <Scene key="mapPage" component={Map} />
          <Scene key="details" component={Details} />
        </Stack>
      </Tabs>
    </Router>
  );
}
