import PropTypes from "prop-types";
import React, { Component, useState } from "react";
import { Slider } from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import {
  Alert,
  Dimensions,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  SafeAreaView
} from "react-native";
import { Button, Icon, Input, Item } from "native-base";
import { LinearGradient } from "expo-linear-gradient";

import mapStyle from "../config/mapStyle.json";

// Config
import colors from "../config/colors";
import typography from "../config/typography";

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20
  },
  modal: {
    flex: 1
  },
  buttonHide: {
    position: "absolute",
    top: 60,
    right: 0,
    zIndex: 4
  },
  icon: {
    color: colors.white
  },
  subtitle: {
    marginTop: 50,
    marginBottom: 20,
    fontFamily: typography.medium,
    fontSize: 22,
    color: colors.white
  },
  text: {
    marginBottom: 20,
    fontFamily: typography.regular,
    fontSize: 18,
    color: colors.white
  },
  mapStyle: {
    width: "100%",
    height: 230
  },
  filtersContainer: {
    flexDirection: "row"
  },
  button: {
    paddingHorizontal: 20,
    marginRight: 10,
    marginBottom: 10,
    borderRadius: 30,
    backgroundColor: colors.white,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.2,
    shadowRadius: 6
  },
  buttonActive: {
    backgroundColor: colors.purple
  },
  buttonText: {
    fontSize: 13
  },
  buttonTextActive: {
    color: colors.white
  }
});

const CustomModal = ({ visible, onClose, onTypeSelect, selectedType }) => (
  <Modal
    animationType="slide"
    transparent={true}
    visible={visible}
    onRequestClose={() => {
      console.log("test");
    }}
  >
    <LinearGradient
      colors={[colors.orange, colors.pink]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      style={[styles.modal, styles.container]}
    >
      <SafeAreaView>
        <TouchableHighlight style={styles.buttonHide} onPress={onClose}>
          <Icon type="FontAwesome5" name="times" style={styles.icon} />
        </TouchableHighlight>

        <View>
          <Text style={styles.subtitle}>Prix</Text>
          <Slider
            style={{ width: "100%", height: 40 }}
            minimumValue={0}
            maximumValue={25}
            minimumTrackTintColor={colors.purple}
            maximumTrackTintColor={colors.white}
          />
        </View>

        <View>
          <Text style={styles.subtitle}>Localisation</Text>
          <Text style={styles.text}>Placer le point sur la carte pour déterminer votre choix</Text>
          <MapView
            style={styles.mapStyle}
            customMapStyle={mapStyle}
            initialRegion={{
              latitude: 47.2183715,
              longitude: -1.553621,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421
            }}
            provider={PROVIDER_GOOGLE}
          ></MapView>
        </View>

        <View>
          <Text style={styles.subtitle}>Ambiance</Text>
          <View style={styles.filtersContainer}>
            <Button
              style={[styles.button, selectedType === null && styles.buttonActive]}
              onPress={() => onTypeSelect(null)}
            >
              <Text style={[styles.buttonText, selectedType === null && styles.buttonTextActive]}>Tous</Text>
            </Button>
            <Button
              style={[styles.button, selectedType === "Outdoor" && styles.buttonActive]}
              onPress={() => onTypeSelect("Outdoor")}
            >
              <Text style={[styles.buttonText, selectedType === "Outdoor" && styles.buttonTextActive]}>Outdoor</Text>
            </Button>
            <Button
              style={[styles.button, selectedType === "Afterwork" && styles.buttonActive]}
              onPress={() => onTypeSelect("Afterwork")}
            >
              <Text style={[styles.buttonText, selectedType === "Afterwork" && styles.buttonTextActive]}>
                Afterwork
              </Text>
            </Button>
          </View>
        </View>
      </SafeAreaView>
    </LinearGradient>
  </Modal>
);

export default CustomModal;
