import PropTypes from "prop-types";
import React from "react";
import { Text, View } from "react-native";

import colors from "../config/colors";

const styles = {
  marker: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: colors.pink,
    borderRadius: "50%"
  }
};

const color = data => {
  if (data.type == "spectacle") {
    return colors.pink;
  }
  if (data.type == "bar") {
    return colors.purple;
  }
  if (data.type == "film") {
    return colors.black;
  }
  if (data.type == "activity") {
    return colors.orange;
  }
  return colors.black;
};

const CustomMarker = ({ type }) => <View style={[styles.marker, { backgroundColor: color({ type }) }]}></View>;

CustomMarker.propTypes = {
  value: PropTypes.string.isRequired
};

export default CustomMarker;
