import PropTypes from "prop-types";
import React, { Component } from "react";
import { Image, Dimensions, StyleSheet } from "react-native";
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, View, Badge } from "native-base";

// Config
import colors from "../config/colors";
import typography from "../config/typography";

const styles = StyleSheet.create({
  card: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
    height: 85,
    borderWidth: 0,
    borderColor: "transparent",
    borderRadius: 20,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.2,
    shadowRadius: 6,
    backgroundColor: colors.white
  },
  imageWrapper: {
    width: 85,
    height: 85,
    borderRadius: 20,
    overflow: "hidden"
  },
  image: {
    width: "100%",
    height: "100%"
  },
  textWrapper: {
    paddingHorizontal: 20
  },
  title: {
    fontSize: 20,
    fontFamily: typography.medium,
    color: colors.black
  },
  subtitle: {
    fontSize: 18,
    fontFamily: typography.regular
  },
  price: {
    fontSize: 18,
    fontFamily: typography.medium,
    color: colors.purple
  },
  icon: {
    fontSize: 20,
    color: colors.black,
    paddingRight: 20
  }
});

export default class CustomMiniCard extends Component {
  render() {
    const { title, subtitle, image, price } = this.props;

    return (
      <Card style={styles.card}>
        <View style={styles.imageWrapper}>
          <Image source={image} style={styles.image} />
        </View>
        <View style={styles.textWrapper}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}>{subtitle}</Text>
          <Text style={styles.price}>{price}</Text>
        </View>
        <Right>
          <Icon type="FontAwesome5" name="arrow-right" style={styles.icon}></Icon>
        </Right>
      </Card>
    );
  }
}

CustomMiniCard.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  image: PropTypes.string,
  price: PropTypes.string
};
