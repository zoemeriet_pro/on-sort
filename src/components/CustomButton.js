import PropTypes from "prop-types";

import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Text } from "native-base";
import { LinearGradient } from "expo-linear-gradient";

// Config
import colors from "../config/colors";
import typography from "../config/typography";

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 20,
    paddingHorizontal: 15,
    paddingVertical: 18,
    borderRadius: 20
  },
  text: {
    fontFamily: typography.bold,
    fontSize: 14,
    color: colors.white,
    textTransform: "uppercase"
  }
});

export default class CustomButton extends Component {
  render() {
    const { text } = this.props;

    return (
      <LinearGradient
        colors={[colors.purple, colors.pink]}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={styles.button}
      >
        <Text style={styles.text}>{text}</Text>
      </LinearGradient>
    );
  }
}

CustomButton.PropTypes = {
  text: PropTypes.string.isRequired
};

CustomButton.defaultProps = {
  text: "Titre"
};
