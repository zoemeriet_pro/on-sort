import React from "react";
import { StyleSheet } from "react-native";
import { Icon } from "native-base";
import { LinearGradient } from "expo-linear-gradient";

import colors from "../config/colors";

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    justifyContent: "center",
    width: 55,
    height: 55,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.2,
    shadowRadius: 6,
    borderRadius: 28
  },
  icon: {
    fontSize: 22,
    color: colors.white
  }
});

const CustomButtonFilters = () => {
  return (
    <LinearGradient
      colors={[colors.orange, colors.pink]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      style={styles.button}
    >
      <Icon type="FontAwesome5" name="sliders-h" style={styles.icon} />
    </LinearGradient>
  );
};

export default CustomButtonFilters;
