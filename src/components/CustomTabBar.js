import React from "react";
import { Dimensions, Image, View, TouchableOpacity, StyleSheet } from "react-native";
import { Footer, FooterTab, Button, Icon, ImageBackground } from "native-base";
import { Actions } from "react-native-router-flux";
import { LinearGradient } from "expo-linear-gradient";

import Images from "../../assets/img";

// Config
import colors from "../config/colors";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  footer: {
    position: "relative",
    height: 65,
    backgroundColor: colors.white
  },
  background: {
    position: "absolute",
    top: -20,
    left: 0,
    width: "100%"
  },
  tab: {
    flex: 1
  },
  button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  icon: {
    fontSize: 25,
    color: colors.black
  },
  iconAcive: {
    color: colors.pink
  }
});

export default class CustomTabBar extends React.Component {
  onTabClick = key => {
    Actions.jump(key);
  };

  render() {
    const { state } = this.props.navigation;
    const activeTabIndex = state.index;

    return (
      <View>
        <Footer style={styles.footer}>
          <Image source={Images["background"]} style={styles.background} />
          <FooterTab style={styles.tab}>
            {state.routes.map((element, index) => (
              <TouchableOpacity key={element.key} onPress={() => this.onTabClick(element.key)} style={styles.button}>
                <Icon
                  type="FontAwesome5"
                  name={element.routes[0].params.icon ? element.routes[0].params.icon : "list-ul"}
                  style={[styles.icon, activeTabIndex === index && styles.iconAcive]}
                />
              </TouchableOpacity>
            ))}
            <TouchableOpacity style={styles.button}>
              <Icon type="FontAwesome5" name="random" style={styles.icon} />
            </TouchableOpacity>
          </FooterTab>
        </Footer>
      </View>
    );
  }
}
