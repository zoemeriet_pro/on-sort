import PropTypes from "prop-types";
import React, { Component } from "react";
import { Image, Dimensions, StyleSheet } from "react-native";
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, View, Badge } from "native-base";

import CustomBadge from "../components/CustomBadge";

// Config
import colors from "../config/colors";
import typography from "../config/typography";

const styles = StyleSheet.create({
  card: {
    marginLeft: 20,
    width: 295,
    height: 207,
    borderWidth: 0,
    borderColor: "transparent",
    shadowOpacity: 0,
    backgroundColor: "transparent"
  },
  cardImage: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-between",
    width: 248,
    height: 166,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.2,
    shadowRadius: 6,
    borderRadius: 20
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    borderRadius: 20
  },
  time: {
    margin: 15,
    padding: 20,
    borderRadius: 20,
    backgroundColor: colors.white
  },
  timeText: {
    fontFamily: typography.light,
    fontSize: 12,
    color: colors.black
  },
  textWrapper: {
    margin: 15
  },
  title: {
    fontFamily: typography.medium,
    fontSize: 20,
    color: colors.white
  },
  subtitle: {
    fontFamily: typography.regular,
    fontSize: 18,
    color: colors.white
  },
  cardPrice: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-end",
    position: "absolute",
    top: 53,
    left: 10,
    zIndex: -1,
    width: 285,
    height: 154,
    borderRadius: 20,
    shadowColor: colors.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 0.2,
    shadowRadius: 6,
    backgroundColor: colors.white
  },
  price: {
    fontFamily: typography.medium,
    fontSize: 18,
    color: colors.purple
  },
  icon: {
    fontSize: 20,
    color: colors.black
  },
  scrollHorizontal: {
    width: "100%"
  }
});

export default class CustomCard extends Component {
  render() {
    const { title, subtitle, image, price, time } = this.props;

    return (
      <Card style={styles.card}>
        <CardItem cardBody style={styles.cardImage}>
          <Image source={image} style={styles.image} />
          <CustomBadge text={time} />
          <View style={styles.textWrapper}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.subtitle}>{subtitle}</Text>
          </View>
        </CardItem>

        <CardItem style={styles.cardPrice}>
          <Text style={styles.price}>{price}</Text>
          <Icon type="FontAwesome5" name="arrow-right" style={styles.icon}></Icon>
        </CardItem>
      </Card>
    );
  }
}

CustomCard.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  image: PropTypes.string,
  price: PropTypes.string,
  time: PropTypes.string
};
