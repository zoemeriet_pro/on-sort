import PropTypes from "prop-types";
import React from "react";
import { StyleSheet, Text } from "react-native";

// Config
import colors from "../config/colors";
import typography from "../config/typography";

const styles = StyleSheet.create({
  title: {
    marginHorizontal: 20,
    fontFamily: typography.medium,
    fontSize: 35,
    color: colors.black
  }
});

const CustomText = ({ text }) => <Text style={styles.title}>{text}</Text>;

CustomText.PropTypes = {
  text: PropTypes.string.isRequired
};

CustomText.defaultProps = {
  text: "Titre"
};

export default CustomText;
