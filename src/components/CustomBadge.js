import PropTypes from "prop-types";
import React from "react";
import { StyleSheet, Text } from "react-native";
import { Badge } from "native-base";

// Config
import colors from "../config/colors";
import typography from "../config/typography";

const styles = StyleSheet.create({
  badge: {
    margin: 15,
    borderRadius: 20,
    backgroundColor: colors.white
  },
  badgeText: {
    marginHorizontal: 10,
    fontFamily: typography.light,
    fontSize: 12,
    color: colors.black
  }
});

const CustomBadge = ({ text }) => (
  <Badge style={styles.badge}>
    <Text style={styles.badgeText}>{text}</Text>
  </Badge>
);

CustomBadge.PropTypes = {
  text: PropTypes.string.isRequired
};

export default CustomBadge;
