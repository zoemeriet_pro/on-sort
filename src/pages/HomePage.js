import React, { useEffect, useState } from "react";
import { Actions } from "react-native-router-flux";
import { StyleSheet, View, TouchableOpacity, ScrollView, SafeAreaView } from "react-native";

import CustomButton from "../components/CustomButton";
import CustomTitle from "../components/CustomTitle";
import CustomCard from "../components/CustomCard";
import CustomButtonFilters from "../components/CustomButtonFilters";
import CustomModal from "../components/CustomModal";

// Config
import colors from "../config/colors";

// Data
import SpectaclesList from "../data/spectacles.json";
import BarsList from "../data/bars.json";
import FilmsList from "../data/films.json";
import ActivitiesList from "../data/activities.json";
import Images from "../../assets/img";

const styles = StyleSheet.create({
  buttonFilters: {
    position: "absolute",
    top: 60,
    right: 20,
    zIndex: 2
  },
  page: {
    paddingTop: 60,
    backgroundColor: colors.grey
  },
  typeList: {
    width: "100%",
    paddingVertical: 20
  },
  type: {
    marginBottom: 50
  },

  filtersContainer: {
    flexDirection: "row",
    marginHorizontal: 20
  }
});

const HomePage = () => {
  const modal = React.useRef();

  const goToListing = (list, title, subtitle) => {
    Actions.listing({
      list,
      title,
      subtitle
    });
  };

  const goToDetails = index => {
    Actions.details({
      index
    });
  };

  const [selectedType, setSelectedType] = useState(null);

  const [filteredSpectacles, setFilteredSpectacles] = useState(SpectaclesList);
  useEffect(() => {
    const newTab = SpectaclesList.filter(item => (!selectedType ? true : item.type === selectedType));
    setFilteredSpectacles(newTab);
  }, [SpectaclesList, selectedType]);

  const [filteredBars, setFilteredBars] = useState(BarsList);
  useEffect(() => {
    const newTab = BarsList.filter(item => (!selectedType ? true : item.type === selectedType));
    setFilteredBars(newTab);
  }, [setFilteredBars, selectedType]);

  const [filteredFilms, setFilteredFilms] = useState(FilmsList);
  useEffect(() => {
    const newTab = FilmsList.filter(item => (!selectedType ? true : item.type === selectedType));
    setFilteredFilms(newTab);
  }, [FilmsList, selectedType]);

  const [filteredActivities, setFilteredActivities] = useState(ActivitiesList);
  useEffect(() => {
    const newTab = ActivitiesList.filter(item => (!selectedType ? true : item.type === selectedType));
    setFilteredActivities(newTab);
  }, [ActivitiesList, selectedType]);

  const [modalVisible, setModalVisible] = useState(false);

  return (
    <SafeAreaView>
      <TouchableOpacity
        onPress={() => {
          setModalVisible(true);
        }}
        style={styles.buttonFilters}
      >
        <CustomButtonFilters />
      </TouchableOpacity>
      <ScrollView>
        <View style={styles.page}>
          {/* SPECTACLES */}
          <View style={styles.type}>
            <CustomTitle text="Spectacles" />

            <ScrollView
              style={styles.typeList}
              horizontal
              snapToInterval="315"
              decelerationRate="fast"
              snapToAlignment="start"
            >
              {filteredSpectacles.map((spectacle, index) => (
                <TouchableOpacity key={index} onPress={() => goToDetails(spectacle)}>
                  <CustomCard
                    title={spectacle.title}
                    subtitle={spectacle.subtitle}
                    image={Images[spectacle.image]}
                    price={spectacle.price}
                    time={spectacle.time}
                  />
                </TouchableOpacity>
              ))}
            </ScrollView>
            <TouchableOpacity onPress={() => goToListing(SpectaclesList, "Spectacles", "Tous les spectacles")}>
              <CustomButton text="Voir tous les spectacles" />
            </TouchableOpacity>
          </View>

          {/* BARS */}
          <View style={styles.type}>
            <CustomTitle text="Bars" />
            <ScrollView
              style={styles.typeList}
              horizontal
              snapToInterval="315"
              decelerationRate="fast"
              snapToAlignment="start"
            >
              {filteredBars.map((bar, index) => (
                <TouchableOpacity key={index} onPress={() => goToDetails(bar)}>
                  <CustomCard
                    title={bar.title}
                    subtitle={bar.subtitle}
                    image={Images[bar.image]}
                    price={bar.price}
                    time={bar.time}
                  />
                </TouchableOpacity>
              ))}
            </ScrollView>
            <TouchableOpacity onPress={() => goToListing(BarsList, "Bars", "Tous les bars")}>
              <CustomButton text="Voir tous les bars" />
            </TouchableOpacity>
          </View>

          {/* FILMS */}
          <View style={styles.type}>
            <CustomTitle text="Films" />
            <ScrollView
              style={styles.typeList}
              horizontal
              snapToInterval="315"
              decelerationRate="fast"
              snapToAlignment="start"
            >
              {filteredFilms.map((film, index) => (
                <TouchableOpacity key={index} onPress={() => goToDetails(film)}>
                  <CustomCard
                    title={film.title}
                    subtitle={film.subtitle}
                    image={Images[film.image]}
                    price={film.price}
                    time={film.time}
                  />
                </TouchableOpacity>
              ))}
            </ScrollView>
            <TouchableOpacity onPress={() => goToListing(FilmsList, "Films", "Tous les films")}>
              <CustomButton text="Voir tous les films" />
            </TouchableOpacity>
          </View>

          {/* ACTIVITÉS */}
          <View style={styles.type}>
            <CustomTitle text="Activités" />
            <ScrollView
              style={styles.typeList}
              horizontal
              snapToInterval="315"
              decelerationRate="fast"
              snapToAlignment="start"
            >
              {filteredActivities.map((activity, index) => (
                <TouchableOpacity key={index} onPress={() => goToDetails(activity)}>
                  <CustomCard
                    title={activity.title}
                    subtitle={activity.subtitle}
                    image={Images[activity.image]}
                    price={activity.price}
                    time={activity.time}
                  />
                </TouchableOpacity>
              ))}
            </ScrollView>
            <TouchableOpacity onPress={() => goToListing(ActivitiesList, "Activités", "Toutes les activités")}>
              <CustomButton text="Voir toutes les activités" />
            </TouchableOpacity>
          </View>
        </View>

        <CustomModal
          ref={modal}
          visible={modalVisible}
          onClose={() => setModalVisible(false)}
          onTypeSelect={type => setSelectedType(type)}
          selectedType={selectedType}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomePage;
