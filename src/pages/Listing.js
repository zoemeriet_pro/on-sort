import PropTypes from "prop-types";
import React from "react";
import { Actions } from "react-native-router-flux";
import { SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

// Components
import CustomTitle from "../components/CustomTitle";
import CustomCard from "../components/CustomCard";
import CustomMiniCard from "../components/CustomMiniCard";

// Config
import colors from "../config/colors";
import typography from "../config/typography";

// Data
import Images from "../../assets/img";

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20
  },
  page: {
    paddingTop: 60,
    paddingBottom: 30,
    backgroundColor: colors.grey
  },
  section: {
    marginTop: 30
  },
  coeur: {
    paddingVertical: 20
  },
  subtitle: {
    marginBottom: 20,
    fontFamily: typography.medium,
    fontSize: 22,
    color: colors.black
  },
  subtitleWhite: {
    color: colors.white
  }
});

const Listing = ({ list, title, subtitle }) => {
  const goToDetails = index => {
    Actions.details({
      index
    });
  };

  if (!list) {
    return null;
  }

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.page}>
          <CustomTitle text={title} />
          <LinearGradient
            colors={[colors.orange, colors.pink]}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={[styles.coeur, styles.container, styles.section]}
          >
            <Text style={[styles.subtitle, styles.subtitleWhite]}>Coup de coeur</Text>

            {list.map((item, index) => {
              if (item.coeur) {
                return (
                  <TouchableOpacity key={index} onPress={() => goToDetails(item)}>
                    <CustomCard
                      title={item.title}
                      subtitle={item.subtitle}
                      image={Images[item.image]}
                      price={item.price}
                      time={item.time}
                    />
                  </TouchableOpacity>
                );
              }
            })}
          </LinearGradient>

          <View style={[styles.container, styles.section]}>
            <Text style={styles.subtitle}>{subtitle}</Text>

            {list.map((item, index) => (
              <TouchableOpacity key={index} onPress={() => goToDetails(item)}>
                <CustomMiniCard
                  title={item.title}
                  subtitle={item.subtitle}
                  image={Images[item.image]}
                  price={item.price}
                />
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

Listing.propTypes = {
  list: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired
};

export default Listing;
