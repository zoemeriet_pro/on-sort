import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { Actions } from "react-native-router-flux";
import {
  Button,
  Dimensions,
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  SafeAreaView
} from "react-native";

import CustomButton from "../components/CustomButton";
import CustomBadge from "../components/CustomBadge";

// Config
import colors from "../config/colors";
import typography from "../config/typography";

// Data
import Images from "../../assets/img";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    width: width,
    height: 300
  },
  page: {
    flex: 1,
    position: "relative",
    marginTop: 200,
    paddingTop: 30,
    paddingBottom: 100,
    backgroundColor: "#FCFCFC",
    borderRadius: 20
  },
  badge: {
    position: "absolute",
    top: -60,
    left: 0
  },
  title: {
    marginBottom: 3,
    fontSize: 30,
    fontFamily: typography.medium
  },
  subtitle: {
    fontSize: 25,
    fontFamily: typography.regular
  },
  titleBlue: {
    marginTop: 25,
    marginBottom: 5,
    fontFamily: typography.medium,
    fontSize: 22,
    color: colors.purple
  },
  text: {
    fontSize: 18,
    fontFamily: typography.regular,
    lineHeight: 23
  },
  button: {
    position: "absolute",
    bottom: 20,
    zIndex: 5,
    width: width
  }
});

const Details = ({ index }) => {
  if (!index) {
    return null;
  }

  return (
    <SafeAreaView>
      <Image source={Images[index.image]} style={styles.image} />
      <TouchableOpacity style={styles.button}>
        <CustomButton text="Réservez" />
      </TouchableOpacity>
      <ScrollView>
        <View style={[styles.page, styles.container]}>
          <View style={styles.badge}>
            <CustomBadge text={index.time} />
          </View>

          <Text style={styles.title}>{index.title}</Text>
          <Text style={styles.subtitle}>{index.subtitle}</Text>

          {/* Quand */}
          <Text style={styles.titleBlue}>Quand ?</Text>
          <Text style={styles.text}>{index.day}</Text>
          <Text style={styles.text}>{index.hour}</Text>

          {/* Combien */}
          <Text style={styles.titleBlue}>Combien ?</Text>
          <Text style={styles.text}>{index.price}</Text>

          {/* Où */}
          <Text style={styles.titleBlue}>Où ?</Text>
          <Text style={styles.text}>{index.address.name}</Text>
          <Text style={styles.text}>{index.address.street}</Text>
          <Text style={styles.text}>
            {index.address.zipCode}, {index.address.city}
          </Text>

          {/* A propos */}
          {index.description ? <Text style={styles.titleBlue}>À propos</Text> : null}
          {index.description ? <Text style={styles.text}>{index.description}</Text> : null}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

Details.propTypes = {
  index: PropTypes.object.isRequired
};

export default Details;
