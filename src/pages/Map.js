import React from "react";
import { Actions } from "react-native-router-flux";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import { Dimensions, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";

import CustomCard from "../components/CustomCard";
import CustomMarker from "../components/CustomMarker";
import mapStyle from "../config/mapStyle.json";

// Data
import SpectaclesList from "../data/spectacles.json";
import BarsList from "../data/bars.json";
import FilmsList from "../data/films.json";
import ActivitiesList from "../data/activities.json";
import Images from "../../assets/img";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  mapStyle: {
    width,
    height
  },
  overlay: {
    width: "100%",
    position: "absolute",
    bottom: 20,
    left: 0,
    paddingHorizontal: 20
  }
});

const Map = () => {
  const goToDetails = index => {
    Actions.details({
      index
    });
  };

  return (
    <View style={styles.container}>
      <MapView
        style={styles.mapStyle}
        customMapStyle={mapStyle}
        initialRegion={{
          latitude: 47.2183715,
          longitude: -1.553621,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}
        provider={PROVIDER_GOOGLE}
      >
        {SpectaclesList.map(spectacle => (
          <Marker
            coordinate={{ latitude: spectacle.address.lat, longitude: spectacle.address.long }}
            title={spectacle.address.name}
          >
            <CustomMarker type="spectacle" />
          </Marker>
        ))}
        {BarsList.map(bar => (
          <Marker coordinate={{ latitude: bar.address.lat, longitude: bar.address.long }} title={bar.address.name}>
            <CustomMarker type="bar" />
          </Marker>
        ))}
        {FilmsList.map(film => (
          <Marker coordinate={{ latitude: film.address.lat, longitude: film.address.long }} title={film.address.name}>
            <CustomMarker type="film" />
          </Marker>
        ))}
        {ActivitiesList.map(activity => (
          <Marker
            coordinate={{ latitude: activity.address.lat, longitude: activity.address.long }}
            title={activity.address.name}
          >
            <CustomMarker type="activity" />
          </Marker>
        ))}
      </MapView>

      <ScrollView style={styles.overlay} horizontal pagingEnabled snapToAlignment="start">
        {SpectaclesList.map((spectacle, index) => (
          <TouchableOpacity key={index} onPress={() => goToDetails(spectacle)}>
            <CustomCard
              title={spectacle.title}
              subtitle={spectacle.subtitle}
              image={Images[spectacle.image]}
              price={spectacle.price}
              time={spectacle.time}
            />
          </TouchableOpacity>
        ))}
        {BarsList.map((bar, index) => (
          <CustomCard
            key={index}
            title={bar.title}
            subtitle={bar.subtitle}
            image={Images[bar.image]}
            price={bar.price}
            time={bar.time}
          />
        ))}
        {FilmsList.map((film, index) => (
          <CustomCard
            key={index}
            title={film.title}
            subtitle={film.subtitle}
            image={Images[film.image]}
            price={film.price}
            time={film.time}
          />
        ))}
        {ActivitiesList.map((activity, index) => (
          <CustomCard
            key={index}
            title={activity.title}
            subtitle={activity.subtitle}
            image={Images[activity.image]}
            price={activity.price}
            time={activity.time}
          />
        ))}
      </ScrollView>
    </View>
  );
};

export default Map;
