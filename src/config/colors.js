const colors = {
  black: "#0C0A2D",
  white: "#FFFFFF",
  grey: "#FCFCFC",
  pink: "#E8488B",
  purple: "#4F4BF1",
  orange: "#F09062"
};

export default colors;
