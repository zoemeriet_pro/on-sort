const typography = {
  light: "rubikLight",
  regular: "rubik",
  medium: "rubikMedium",
  bold: "rubikBold"
};

export default typography;
